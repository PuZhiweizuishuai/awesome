package cn.mgdream.awesome.file.parse.service;

import cn.mgdream.awesome.file.parse.domain.Parsed;

import java.util.stream.Stream;

public interface ParsedService {

    void save(Stream<Parsed> parsedStream);
}
