package cn.mgdream.awesome.file.storage.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface FileStorageService {

    void store(MultipartFile file) throws IOException;

    Stream<Path> loadAll() throws IOException;

    Path load(String filename) throws FileNotFoundException;

    void update(MultipartFile file) throws IOException;

    void delete(String filename) throws IOException;
}
