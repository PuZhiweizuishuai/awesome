package cn.mgdream.awesome.verify.code.service.impl;

import cn.mgdream.awesome.verify.code.exception.VerifyFailedException;
import cn.mgdream.awesome.verify.code.repository.VerifyCodeRepository;
import cn.mgdream.awesome.verify.code.service.GenerateImageService;
import cn.mgdream.awesome.verify.code.service.SendMessageService;
import cn.mgdream.awesome.verify.code.service.VerifyCodeService;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.Objects;
import java.security.SecureRandom;

@Service
public class DigitsVerifyCodeServiceImpl implements VerifyCodeService {

    private static final int VERIFY_CODE_LENGTH = 4;

    private static final long VERIFY_CODE_EXPIRE_TIMEOUT = 60000L;

    private final VerifyCodeRepository verifyCodeRepository;

    private final GenerateImageService generateImageService;

    private final SendMessageService sendMessageService;

    public DigitsVerifyCodeServiceImpl(VerifyCodeRepository verifyCodeRepository,
                                       GenerateImageService generateImageService,
                                       SendMessageService sendMessageService) {
        this.verifyCodeRepository = verifyCodeRepository;
        this.generateImageService = generateImageService;
        this.sendMessageService = sendMessageService;
    }

    private static String randomDigitString(int length) {
        StringBuilder stringBuilder = new StringBuilder();
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < length; i++) {
            stringBuilder.append(random.nextInt(10));
        }
        return stringBuilder.toString();
    }

    private static String appendTimestamp(String string) {
        return string + "#" + System.currentTimeMillis();
    }

    @Override
    public void send(String key) {
        String verifyCode = randomDigitString(VERIFY_CODE_LENGTH);
        String verifyCodeWithTimestamp = appendTimestamp(verifyCode);
        verifyCodeRepository.save(key, verifyCodeWithTimestamp);
        sendMessageService.send(key, verifyCode);
    }

    @Override
    public void verify(String key, String code) {
        String lastVerifyCodeWithTimestamp = verifyCodeRepository.find(key);
        String[] lastVerifyCodeAndTimestamp = lastVerifyCodeWithTimestamp.split("#");
        String lastVerifyCode = lastVerifyCodeAndTimestamp[0];
        long timestamp = Long.parseLong(lastVerifyCodeAndTimestamp[1]);
        if (timestamp + VERIFY_CODE_EXPIRE_TIMEOUT < System.currentTimeMillis()
                || !Objects.equals(code, lastVerifyCode)) {
            throw new VerifyFailedException("Invalid code for " + key);
        }
    }

    @Override
    public Image image(String key) {
        String verifyCode = randomDigitString(VERIFY_CODE_LENGTH);
        String verifyCodeWithTimestamp = appendTimestamp(verifyCode);
        Image image = generateImageService.imageWithDisturb(verifyCode);
        verifyCodeRepository.save(key, verifyCodeWithTimestamp);
        return image;
    }
}
